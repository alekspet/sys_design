package distr.hash.node;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.IntPredicate;
import java.util.function.IntUnaryOperator;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class HashNodeTest {
    public List<Integer> findDisappearedNumbers(int[] nums) {
        ArrayList<Integer> res = new ArrayList<>();
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != idx(nums[i])) {
                if (nums[idx(nums[i])] != idx(nums[i])) {
                    int tmp = nums[idx(nums[i])];
                    nums[idx(nums[i])] = nums[i];
                    nums[i] = tmp;
                }
            }
        }
        for (int i = 0; i < nums.length; i++) {
            if (i != nums[i]) {
                res.add(i + 1);
            }
        }
        return res;
    }

    int idx(int x) {
        return x - 1;
    }

    @Test
    public void foo() {
        findDisappearedNumbers(new int[]{4, 3, 2, 7, 8, 2, 3, 1});
    }

    @Test
    public void nodeIdJustCreated() {
        HashNode<Long, String, String> node = new ChordHashNode<>();
        assertNull(node.nodeId());
    }

    @Test
    public void nodeIdJoined() {
        HashNode<Long, String, String> node = new ChordHashNode<>();
        assertThat(node.nodeId(), is(1L));
    }

    @Test
    public void storeRetrieve() {
        HashNode<Long, String, String> node = new ChordHashNode<>();
        String KEY = "key";
        String VALUE = "value";
        node.store(KEY, VALUE);
        assertThat(node.retrieve(KEY), is(VALUE));
    }


    @Test
    public void join() {
        HashNode<Long, String, String> node = new ChordHashNode<>();
        HashNode<Long, String, String> peer = new ChordHashNode<>();
    }

    @Test
    public void leave() {
        HashNode<Long, String, String> node = new ChordHashNode<>();
    }
}