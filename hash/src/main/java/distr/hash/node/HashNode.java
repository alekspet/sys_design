package distr.hash.node;


public interface HashNode<T, K, V> {

    T nodeId();

    void assignId(T id);

    void store(K key, V value);

    V retrieve(K key);

    boolean join(HashNode<? extends T, K, V> networkNode);

    boolean leave();

}