package distr.hash.node;

import java.util.HashMap;
import java.util.Map;

public class ChordHashNode<K, V> implements HashNode<Long, K, V> {

    private Long id;
    private Map<K, V> segment;

    public ChordHashNode(Long id, Map<K, V> segment) {
        this.id = id;
        this.segment = segment;
    }

    public ChordHashNode(Map<K, V> segment) {
        this.segment = segment;
    }

    public ChordHashNode() {
        this.segment = new HashMap<>();
    }

    @Override
    public Long nodeId() {
        return id;
    }

    @Override
    public void assignId(Long id) {
        this.id = id;
    }

    @Override
    public void store(K key, V value) {
        segment.put(key, value);
    }

    @Override
    public V retrieve(K key) {
        return segment.get(key);
    }

    @Override
    public boolean join(HashNode<? extends Long, K, V> networkNode) {
        return false;
    }

    @Override
    public boolean leave() {
        return false;
    }
}
